﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulleyPulley : MonoBehaviour
{
    public Rigidbody Ladder;
    public BoxCollider Collider;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 point = Collider.bounds.ClosestPoint(transform.position);
        if (Vector3.Distance(point, transform.position) > 1) {
            Ladder.AddForceAtPosition((transform.position - point), point, ForceMode.Impulse);
            // ForceMode.
        }
    }
}
