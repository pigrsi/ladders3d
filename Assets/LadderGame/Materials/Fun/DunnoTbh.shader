﻿Shader "Unlit/DunnoTbh"
{
    Properties
    {
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float progress = _Time.y % 1;
                float r = 1 - abs(1 - 2 * progress);
                progress = (_Time.y + 0.3) % 1;
                float g = 1 - abs(1 - 2 * progress);
                progress = (_Time.y + 0.6) % 1;
                float b = 1 - abs(1 - 2 * progress);
                fixed4 color = fixed4(r, g, b, 1);
                return color;
            }
            ENDCG
        }
    }
}
