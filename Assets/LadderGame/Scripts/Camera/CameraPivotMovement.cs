﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPivotMovement : MonoBehaviour
{
    public void MoveSide(float amount) {
        transform.Rotate(Vector3.up, amount * 140 * Time.deltaTime, Space.World);
    }

    public static void Zoom(Transform camera, float amount) {
        camera.position += camera.forward.normalized * amount;
    }
}
