﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMain : MonoBehaviour
{
    [SerializeField] Transform cameraPivot = null;
    [SerializeField] ClimberController climberController = null;
    CameraPivotMovement cameraPivotMovement;
    private GameInputStatus _inputStatus;
    private GameplayInput _gameplayInput;
    private Transform _camera;

    void Awake() {
        _camera = cameraPivot.GetChild(0);
        _inputStatus = new GameInputStatus(_camera);
        cameraPivotMovement = cameraPivot.GetComponent<CameraPivotMovement>();

        _gameplayInput = new GameplayInput(_inputStatus);
        climberController.SetInputStatusObject(_gameplayInput.InputStatus);

    }
    

    void Update()
    {
        // GameplayInput playerInput = GetComponent<GameplayInput>();
        // ClimberController.PlayerInputs playerInputs = new ClimberController.PlayerInputs();
        // playerInputs.RawMoveAxis = GameplayInput.GetRawMoveAxis();
        // playerInputs.CamMoveAxis = GameplayInput.AdjustRawMoveAxisForCam(cam, playerInputs.RawMoveAxis);
        // playerInputs.Jump = playerInput.GetJumpInput() == 1;
        // playerInputs.Grab = playerInput.GetUseInput() == 1;
        // climberController.UpdateInputs(playerInputs);

        _gameplayInput.Poll();

        // float cameraInput = playerInput.GetCameraSideInput();
        // float cameraZoom = playerInput.GetCameraZoomInput();
        if (_inputStatus.SecondaryAxis.x != 0) cameraPivotMovement.MoveSide(_inputStatus.SecondaryAxis.x);
        if (_inputStatus.Zoom != 0) CameraPivotMovement.Zoom(_camera, _inputStatus.Zoom);

        _gameplayInput.ConsumeCameraInput();
    }

    void OnDestroy() {
        _gameplayInput.OnDestroy();
    }
}
