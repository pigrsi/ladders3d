﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSides : MonoBehaviour
{
    float swapCounter = 3;
    float modifier = 1;

    // Update is called once per frame
    void Update()
    {
        if (swapCounter <= 0) {
            modifier *= -1;
            swapCounter = 3;
        } else {
            swapCounter -= Time.deltaTime;
        }

        Vector3 pos = transform.position;
        pos += Vector3.forward * modifier * 4 * Time.deltaTime;
        transform.position = pos;
    }
}
