﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyForce : MonoBehaviour
{
    void OnCollisionEnter(Collision collision) {
        collision.rigidbody.velocity = collision.rigidbody.velocity * -1;
    }
}
