﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launch : MonoBehaviour
{
    public Vector3 velocity;

    void OnCollisionEnter(Collision collision) {
        collision.rigidbody.velocity = velocity;
    }
}
