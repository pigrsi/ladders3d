﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(order=51)]
public class DebugSettings : ScriptableObject
{
    public bool ShowSurfaceTangents = true;
}
