﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour
{
    [SerializeField] DebugSettings DevDebugSettings = null;
    [SerializeField] DebugSettings ReleaseDebugSettings = null;
    [HideInInspector] public DebugSettings DebugSettings;

    void Start() {
        this.DebugSettings = Debug.isDebugBuild ? DevDebugSettings : ReleaseDebugSettings;
    }
}
