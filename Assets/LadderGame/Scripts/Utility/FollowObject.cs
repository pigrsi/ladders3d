﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour
{
    [SerializeField] Transform follow = null;

    // Update is called once per frame
    void Update()
    {
        transform.position = follow.transform.position; 
    }
}
