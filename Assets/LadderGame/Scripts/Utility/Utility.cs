﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utility
{
    public static bool IsGrabbable(Transform obj) {
        return obj.tag == "Grabbable";
    }
}
