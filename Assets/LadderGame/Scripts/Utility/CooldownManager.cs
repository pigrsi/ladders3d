﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CooldownManager
{
    private List<Cooldown> Cooldowns = new List<Cooldown>();

    public void Add(string name, float seconds) {
        Cooldown existing = Get(name);
        if (existing != null) {
            existing.seconds = seconds;
            existing.initial = seconds;
            return;
        }
        Cooldowns.Add(new Cooldown(name, seconds));
    }

    public float GetProgress(string name) {
        foreach (Cooldown c in Cooldowns) {
            if (c.name.Equals(name)) return c.GetProgress();
        }
        return 1;
    }

    private Cooldown Get(string name) {
        foreach (Cooldown c in Cooldowns) {
            if (c.name.Equals(name)) return c;
        }
        return null;
    }

    public bool IsActive(string name) {
        foreach (Cooldown c in Cooldowns) {
            if (c.name.Equals(name)) return true;
        }
        return false;
    }

    public void Tick(float delta) {
        if (Cooldowns.Count == 0) return;
        for (int i = Cooldowns.Count-1; i >= 0; --i) {
            Cooldowns[i].seconds -= delta;
            if (Cooldowns[i].seconds <= 0) Cooldowns.RemoveAt(i);
        }
    }

    private class Cooldown {
        public string name;
        public float seconds;
        public float initial;

        public Cooldown(string name, float seconds) {
            this.name = name;
            this.seconds = seconds;
            this.initial = seconds;
        }

        /// <summary>Returns timer progress as value between 0 and 1 (inclusive)</summary>
        public float GetProgress() {
            return Mathf.Clamp01(1f - (seconds / initial));
        }
    }
}
