﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayFixedDistanceFrom : MonoBehaviour
{

    float magnitude;
    [SerializeField] Transform keepDistanceFrom = null;

    void Start() {
        magnitude = Vector3.Distance(keepDistanceFrom.transform.position, transform.position);
    }

    void Update()
    {
        Vector3 diff = transform.position - keepDistanceFrom.transform.position;
        Vector3 desiredDiff = Vector3.Normalize(diff) * magnitude;
        transform.position = keepDistanceFrom.transform.position + desiredDiff;
    }
}
