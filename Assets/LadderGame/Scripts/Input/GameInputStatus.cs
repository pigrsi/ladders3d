﻿using UnityEngine;

public class GameInputStatus
{
    private Transform _camera;
    public GameInputStatus(Transform camera) {
        _camera = camera;
    }

    public class Button {
        public bool Pressed = false;
        public bool Disabled = false;
    }

    public float Rotate;
    public Vector2 MainAxis = Vector2.zero;
    public Vector2 SecondaryAxis = Vector2.zero;
    public Button Jump = new Button();
    public Button Grab = new Button();
    
    public float Zoom = 0f;

    public Vector2 MainAxisCameraAdjusted() {
        Vector3 camF = _camera.forward;
        Vector3 camR = _camera.right;
        camF.y = 0;
        camR.y = 0;
        camF = camF.normalized;
        camR = camR.normalized;

        Vector3 adjusted = camF*MainAxis.y + camR*MainAxis.x;
        return new Vector2(adjusted.x, adjusted.z);
    }

    public void SetButtonValue(Button button, bool value) {
        if (button.Disabled) {
            if (!value) button.Disabled = false;
            button.Pressed = false;
        } else {
            button.Pressed = value;
        }
    }

    public void ConsumeCameraInput() {
        Zoom = 0f;
    }

    public void DisableButtonUntilRelease(Button button) {
        button.Disabled = true;
        button.Pressed = false;
    }
}
