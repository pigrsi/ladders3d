﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameplayInput
{
    public GameInputStatus InputStatus;

    public GameplayInput(GameInputStatus inputStatusObject) {
        InputStatus = inputStatusObject;
        SetupBindings();
    }
    
    private GameControls _gameControls;

    public void SetupBindings() {
        // Cursor.lockState = CursorLockMode.Locked;

        _gameControls = new GameControls();
        _gameControls.Gameplay.Jump.performed += JumpAction;
        _gameControls.Gameplay.Grab.performed += GrabAction;
        _gameControls.Gameplay.Zoom.performed += ZoomAction;
        _gameControls.Gameplay.Rotate.performed += RotateAction;

        _gameControls.Enable();
    }

    public void OnDestroy() {
        _gameControls.Gameplay.Jump.performed -= JumpAction;
        _gameControls.Gameplay.Grab.performed -= GrabAction;
        _gameControls.Gameplay.Zoom.performed -= ZoomAction;
        _gameControls.Gameplay.Rotate.performed -= RotateAction;
    }

    public void Poll() {
        InputStatus.MainAxis = _gameControls.Gameplay.MainAxis.ReadValue<Vector2>();
        InputStatus.SecondaryAxis = _gameControls.Gameplay.SecondaryAxis.ReadValue<Vector2>();
    }

    /// <summary>Just resets the input values that should be reset every frame</summary>
    public void ConsumeCameraInput() {
        InputStatus.ConsumeCameraInput();
    }

    public void JumpAction(InputAction.CallbackContext ctx) {
        InputStatus.SetButtonValue(InputStatus.Jump, ctx.ReadValue<float>() == 1);
    }

    public void GrabAction(InputAction.CallbackContext ctx) {
        InputStatus.SetButtonValue(InputStatus.Grab, ctx.ReadValue<float>() == 1);
    }

    public void ZoomAction(InputAction.CallbackContext ctx) {
        InputStatus.Zoom = ctx.ReadValue<float>();
    }

    public void RotateAction(InputAction.CallbackContext ctx) {
        InputStatus.Rotate = ctx.ReadValue<float>();
    }
}
