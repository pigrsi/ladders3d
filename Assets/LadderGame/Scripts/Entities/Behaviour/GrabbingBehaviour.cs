﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbingBehaviour
{
    [SerializeField] DetectGrabbable[] GrabDetectors;
    private Transform GrabbedObject;

    public GrabbingBehaviour(DetectGrabbable[] grabDetectors) {
        GrabDetectors = grabDetectors;
    }

    public Transform GetLiftableIfExists(ICharacterInfo c) {
        // All colliders must be touching the same object to grab it
        foreach (DetectGrabbable detector in GrabDetectors) {
            List<Transform> grabbables = detector.GetGrabbables();
            foreach (Transform grabbable in grabbables) {
                if (AllDetectorsTouchGrabbable(grabbable) && CharacterIsInLiftPosition(c, grabbable)) {
                    return grabbable;
                }
            }
        }
        return null;
    }

    private bool CharacterIsInLiftPosition(ICharacterInfo c, Transform grabbable) {
        // Make sure char and grabbable are parallel enough
        Vector3 charFwdFlat = Vector3.ProjectOnPlane(c.CharacterForward(), Vector3.up);
        Vector3 grabFwdFlat = Vector3.ProjectOnPlane(grabbable.forward, Vector3.up);
        if (Vector3.Cross(charFwdFlat, grabFwdFlat).magnitude > 0.5f) return false;

        // Make sure char must be on either end of the grabbable
        float halfLength = grabbable.GetComponent<LadderInfo>().ZLength / 2;
        Vector3 centreToEnd = grabbable.position - (grabbable.position + grabbable.forward * halfLength);
        float minDistance = Vector3.ProjectOnPlane(centreToEnd, Vector3.up).magnitude;
        minDistance += 0.5f;
        Debug.Log("Mydis: " + (c.GetTransform().position - grabbable.position).magnitude + " MinDis: " + minDistance);
        if ((c.GetTransform().position - grabbable.position).magnitude < minDistance) return false;

        return true;
    }

    public void GrabGrabbable(Transform obj) {
        GrabbedObject = obj;
    }

    public Transform GetHeldObject() {
        return GrabbedObject;
    }

    public bool IsHoldingObject() {
        return GrabbedObject != null;
    }

    private bool AllDetectorsTouchGrabbable(Transform grabbable) {
        foreach (DetectGrabbable detector in GrabDetectors) {
            if (!detector.HasDetectedGrab(grabbable)) return false;
        }
        return true;
    }
}
