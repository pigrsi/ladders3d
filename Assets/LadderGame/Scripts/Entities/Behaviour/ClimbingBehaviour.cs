﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbingBehaviour
{
    BoxCollider Mount;
    BoxCollider ClimbRangeTrigger;
    bool WithinMount;
    bool WithinClimbRange;
    float LadderProgress = 0;
    float LadderSinglePieceLength;
    Transform ClimbingObject;
    Vector3 UnmountVelocity;
    Vector3 UnmountPosition;
    Rigidbody LadderRigidbody;
    bool MountedWhileUpsideDown;

    public ClimbingBehaviour(Transform climbingObject) {
        ClimbingObject = climbingObject;
    }

    public bool IsStillWithinGripRange() {
        if (LadderRigidbody.velocity.magnitude > 3) return WithinClimbRange;
        else return WithinMount;
    }

    public void OnTriggerEnter(Collider other) {
        if (other == Mount) WithinMount = true;
        else if (other == ClimbRangeTrigger) WithinClimbRange = true;
    }

    public void OnTriggerStay(Collider other) {
        OnTriggerEnter(other);
    }

    public void OnTriggerExit(Collider other) {
        if (other == Mount) WithinMount = false;
        else if (other == ClimbRangeTrigger) WithinClimbRange = false;
    }

    public bool IsCurrentlyMounted() {
        return Mount != null;
    }

    public void MountLadder(Collider mount) {
        Mount = (BoxCollider) mount;
        LadderRigidbody = Mount.GetComponent<ClimbTriggerInfo>().Body;
        SetLadderValues();
    }

    public void UnmountLadder() {
        UnmountVelocity = LadderRigidbody.velocity;
        // LadderRigidbody.AddForce((LadderRigidbody.transform.position - ClimbingObject.transform.position).normalized * 10, ForceMode.Impulse);
        LadderRigidbody.AddForce(Vector3.down * 10, ForceMode.Impulse);
        Mount = null;
    }

    public Vector3 GetUnmountVelocity() {
        return UnmountVelocity;
    }

    /// <summary>Moves object forward or back on the ladder. Degree is speed percentage.</summary>
    public void MoveAlong(float degree, float maxClimbSpeed) {
        int multiplier = MountedWhileUpsideDown ? -1 : 1;
        LadderProgress += Mathf.Clamp(degree, -1, 1) * maxClimbSpeed * multiplier * Time.deltaTime;
    }

    public Collider GetMount() {
        return Mount;
    }

    public void SetLadderValues() {
        MountedWhileUpsideDown = MountIsCurrentlyUpsideDown();
        WithinMount = true;
        WithinClimbRange = true;
        // From climbing trigger
        ClimbTriggerInfo trigInfo = Mount.GetComponent<ClimbTriggerInfo>();
        LadderSinglePieceLength = trigInfo.SinglePieceLength;
        Vector3 landingPoint = Mount.ClosestPointOnBounds(ClimbingObject.transform.position);
        LadderProgress = Vector3.Distance(landingPoint, GetLadderStartPoint());
        // From main ladder object (top of hierarchy)
        GameObject ladder = trigInfo.LadderMain;
        ClimbRangeTrigger = ladder.GetComponent<LadderInfo>().ClimbRangeTrigger;
    }

    private bool MountIsCurrentlyUpsideDown() {
        return Mount.transform.forward.y < 0;
    }

    public Vector3 GetLadderSnapPosition() {
        return GetLadderStartPoint() + (Mount.transform.forward * LadderProgress) + (LadderRigidbody.velocity * Time.deltaTime);
    }

    private Vector3 GetLadderStartPoint() {
        Vector3 center = Mount.bounds.center;
        float length = Mount.size.z;

        Vector3 ladderStart = center - Mount.transform.forward * (length / LadderSinglePieceLength);
        return ladderStart += Mount.transform.up / 2;
    }

    public Quaternion GetMountedRotation() {
        int multiplier = MountedWhileUpsideDown ? -1 : 1;
        return Quaternion.LookRotation(Mount.transform.up * -1, Mount.transform.forward * multiplier);
    }

    public Vector3 GetMountedLookVector() {
        int multiplier = MountedWhileUpsideDown ? -1 : 1;
        return Mount.transform.forward * multiplier;
    }
}
