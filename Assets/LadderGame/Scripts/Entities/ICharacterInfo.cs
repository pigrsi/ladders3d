﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICharacterInfo
{
    Transform GetTransform();
    Vector3 CharacterUp();
    Vector3 CharacterForward();
    Vector3 CharacterRight();
}
