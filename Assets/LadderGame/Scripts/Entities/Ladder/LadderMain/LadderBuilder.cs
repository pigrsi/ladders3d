﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderBuilder : MonoBehaviour
{
    [SerializeField] Transform meshPrefab = null;
    [SerializeField] Transform climbTriggerPrefab = null;
    [SerializeField] Transform climbRangeTriggerPrefab = null;
    const float SinglePieceLength = 2;

    void Start() {
        SpawnMeshes();
        SpawnParts();
    }

    void SpawnMeshes() {
        BoxCollider boxCollider = GetComponent<BoxCollider>();
        float ladderLength = boxCollider.size.z;
        int piecesRequired = Mathf.RoundToInt(ladderLength / SinglePieceLength);
        Vector3 spawnPoint = boxCollider.transform.position - (boxCollider.transform.forward * ladderLength/2);
        spawnPoint += boxCollider.transform.forward * SinglePieceLength/2;
        for (int i = 0; i < piecesRequired; ++i) {
            Transform child = Instantiate(meshPrefab, spawnPoint, transform.rotation);
            child.SetParent(transform); 
            spawnPoint += boxCollider.transform.forward * SinglePieceLength;
        }
    }

    void SpawnParts() {
        BoxCollider boxCollider = GetComponent<BoxCollider>();

        SpawnClimbRangeTrigger(boxCollider);
        // Front trigger
        SpawnClimbTrigger(boxCollider, transform.position + transform.up * 0.3f, transform.rotation);
        // Back trigger
        Quaternion backTriggerRotation = transform.rotation;
        backTriggerRotation.SetLookRotation(transform.forward, transform.up * -1);
        SpawnClimbTrigger(boxCollider, transform.position - transform.up * 0.3f, backTriggerRotation);
    }

    /// <summary>If climber leaves this trigger they should be forced to unmount.</summary>
    private void SpawnClimbRangeTrigger(BoxCollider ladderCollider) {
        Transform climbRangeTrigger = Instantiate(climbRangeTriggerPrefab, transform.position, transform.rotation);
        climbRangeTrigger.SetParent(transform);
        climbRangeTrigger.GetComponent<BoxCollider>().size = new Vector3(3f, 3f, ladderCollider.size.z);
        LadderInfo info = GetComponent<LadderInfo>();
        info.ClimbRangeTrigger = climbRangeTrigger.GetComponent<BoxCollider>();
        info.ZLength = ladderCollider.size.z;
    }

    /// <summary>A climber uses these to know where ladders are and climbs up these.</summary>
    private void SpawnClimbTrigger(BoxCollider ladderCollider, Vector3 position, Quaternion rotation) {
        Transform climbTrigger = Instantiate(climbTriggerPrefab, transform.position, rotation);
        climbTrigger.SetParent(transform);
        climbTrigger.transform.position = position;
        climbTrigger.GetComponent<BoxCollider>().size = new Vector3(1f, 0.75f, ladderCollider.size.z - (SinglePieceLength / 2));
        ClimbTriggerInfo info = climbTrigger.GetComponent<ClimbTriggerInfo>();
        info.Body = GetComponent<Rigidbody>();
        info.SinglePieceLength = SinglePieceLength;
        info.LadderMain = gameObject;
    }
}
