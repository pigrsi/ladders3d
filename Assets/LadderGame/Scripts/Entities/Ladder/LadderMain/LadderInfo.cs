﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderInfo : MonoBehaviour
{
    [HideInInspector]
    public BoxCollider ClimbRangeTrigger;
    public float ZLength;
}
