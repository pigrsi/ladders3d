﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbTriggerInfo : MonoBehaviour
{
    [HideInInspector]
    public GameObject LadderMain;
    [HideInInspector]
    public Rigidbody Body;
    [HideInInspector]
    public float SinglePieceLength;
}
