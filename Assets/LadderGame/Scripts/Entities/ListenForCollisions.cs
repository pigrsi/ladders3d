﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListenForCollisions : MonoBehaviour
{
    public delegate void OnCollision (Collision collision);
    private OnCollision _onCollision;
    private OnCollision _onCollisionStay;

    public void SetCallbacks(OnCollision onCollision, OnCollision onCollisionStay) {
        _onCollision = onCollision;
        _onCollisionStay = onCollisionStay;
    }

    void OnCollisionEnter(Collision collision) {
        _onCollision(collision);
    }

    void OnCollisionStay(Collision collision) {
        _onCollisionStay(collision);
    }
}
