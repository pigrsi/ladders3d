﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KinematicCharacterController;

public class ClimberController : MonoBehaviour, ICharacterController, ICharacterInfo
{
    public Transform ModelRoot;
    public ClimberState State = new ClimberState();
    public GrabbingBehaviour GrabBehaviour;
    public Transform HandPivot;
    [HideInInspector] public Transform LeftHand;
    [HideInInspector] public Transform RightHand;
    [SerializeField] GameSettings Settings = null;

    [Header("MovementProperties")]
    public bool UsePhysicsGravity = false;
    public float ClimberGravity = -15f;
    public float JumpSpeed = 10f;
    public float MaxClimbSpeed = 10f;
    public float MaxWalkSpeed = 10f;
    public float MaxAirSpeed = 10f;
    public float AirControlDegree = 0.4f;
    // public float ForcePush = 3f;
    [Header("Lifting")]
    public float MaxLiftingWalkSpeed = 10f;
    public float LiftingWalkAcceleration = 90f;
    public float MaxLiftingRotateSpeed = 100f;
    public float LiftingRotateAcceleration = 200f;
    [Header("For Behaviours")]
    public DetectGrabbable[] GrabRangeDetectors = null;
    [Header("KCC")]
    public KinematicCharacterMotor Motor;

    public CooldownManager Cooldowns = new CooldownManager();
    public Quaternion Rotation = Quaternion.identity;
    public Vector3 Position = new Vector3();
    private Vector3 velocity = new Vector3();
    public Vector3 Velocity
    {
        get { return velocity; }
        set { velocity = value; }
    }
    public Vector3 PreviousPosition = new Vector3();
    public Vector3 Gravity;

    public ClimberStateBehaviour CurrentStateController;
    public ClimberIdleBehaviour IdleController;
    public ClimberWalkingBehaviour WalkingController;
    public ClimberJumpingBehaviour JumpingController;
    public ClimberFallingBehaviour FallingController;
    public ClimberClimbingBehaviour ClimbingController;
    public ClimberDraggingBehaviour DraggingController;
    public ClimberLiftingBehaviour LiftingController;

    private GameInputStatus _inputStatus;

    public struct PlayerInputs
    {
        public Vector2 CamMoveAxis;
        public Vector2 RawMoveAxis;
        public bool Jump;
        public bool Grab;
    }
    public struct ClimberRotateCommands
    {
        public bool ReorientSelf;
    }
    public ClimberRotateCommands RotateCommands;

    public class ClimberNextMoveData
    {
        public ClimberNextMoveData(CooldownManager cooldowns)
        {
            Cooldowns = cooldowns;
        }

        // Axes
        public Vector2 CamMoveAxis;
        public Vector2 RawMoveAxis;
        public float RotateAxis;
        // Player requests
        public bool JumpRequested;
        public bool GrabRequested;
        // Climber state
        public Vector3 Velocity;
        public bool StableOnGround;
        public bool GroundHit;
        public Collider ClimbCandidate;
        public Transform LiftCandidate;
        public bool MustUnmount;
        public bool IsUpright;
        // Static
        public CooldownManager Cooldowns;

        /// <summary>Anything not in here should be updated every frame</summary>
        public void Refresh()
        {
            JumpRequested = false;
            GrabRequested = false;
            GroundHit = false;
            ClimbCandidate = null;
            MustUnmount = false;
        }
    }
    public ClimberNextMoveData NextMoveData;

    void Awake()
    {
        LeftHand = HandPivot.GetChild(0);
        RightHand = HandPivot.GetChild(1);

        Motor.CharacterController = this;

        IdleController = new ClimberIdleBehaviour(this);
        WalkingController = new ClimberWalkingBehaviour(this);
        JumpingController = new ClimberJumpingBehaviour(this);
        FallingController = new ClimberFallingBehaviour(this);
        ClimbingController = new ClimberClimbingBehaviour(this);
        LiftingController = new ClimberLiftingBehaviour(this);
        OnEnterState(State.Current);

        NextMoveData = new ClimberNextMoveData(Cooldowns);
        GrabBehaviour = new GrabbingBehaviour(GrabRangeDetectors);

        Gravity = new Vector3(0, ClimberGravity, 0);
    }

    Vector3 FindNewVelocity(Vector3 currentVelocity, float delta)
    {
        Velocity = currentVelocity;
        // Requested moves
        // Change to appropriate state
        CheckInputs();
        UpdateNextMoveClimberState();
        ClimberStates prevState = State.GetCurrentState();
        ClimberStates newState = State.SetStateFromMoveData(NextMoveData);
        if (newState != ClimberStates.Null)
        {
            OnExitState(prevState);
            OnEnterState(newState);
        };

        if (CurrentStateController != null) CurrentStateController.Update(delta);

        NextMoveData.Refresh();
        return Velocity;
    }

    public Transform GetTransform() {
        return transform;
    }

    void FixedUpdate()
    {
        if (CurrentStateController != null) CurrentStateController.FixedUpdate();
    }

    private void OnExitState(ClimberStates state)
    {
        if (state == ClimberStates.Climbing) ClimbingController.Unmount(NextMoveData.JumpRequested);
        else if (state == ClimberStates.Lifting)
        {
            LiftingController.DropLadder();
            _inputStatus.DisableButtonUntilRelease(_inputStatus.Grab);
        }
    }

    private void OnEnterState(ClimberStates state)
    {
        Debug.Log("Entered state: " + state);
        if (state == ClimberStates.Climbing) ClimbingController.JustEnteredState(NextMoveData.ClimbCandidate);
        else if (state == ClimberStates.Lifting)
        {
            LiftingController.StartGrab(NextMoveData.LiftCandidate);
            _inputStatus.DisableButtonUntilRelease(_inputStatus.Grab);
        }

        switch (state)
        {
            case ClimberStates.Idle: CurrentStateController = IdleController; break;
            case ClimberStates.Walking: CurrentStateController = WalkingController; break;
            case ClimberStates.Jumping: CurrentStateController = JumpingController; break;
            case ClimberStates.Falling: CurrentStateController = FallingController; break;
            case ClimberStates.Lifting: CurrentStateController = LiftingController; break;
            case ClimberStates.Climbing: CurrentStateController = ClimbingController; break;
            default: CurrentStateController = null; break;
        }

    }

    private void CheckInputs()
    {
        NextMoveData.JumpRequested = _inputStatus.Jump.Pressed;
        NextMoveData.GrabRequested = _inputStatus.Grab.Pressed;
        NextMoveData.CamMoveAxis = _inputStatus.MainAxisCameraAdjusted();
        NextMoveData.RawMoveAxis = _inputStatus.MainAxis;
        NextMoveData.RotateAxis = _inputStatus.Rotate;
    }

    public void UpdateNextMoveClimberState()
    {
        NextMoveData.StableOnGround = Motor.GroundingStatus.IsStableOnGround;
        NextMoveData.Velocity = Velocity;
        NextMoveData.LiftCandidate = GrabBehaviour.GetLiftableIfExists(this);
        NextMoveData.IsUpright = ClimberIsUpright();
        if (State.IsInState(ClimberStates.Climbing))
        {
            NextMoveData.MustUnmount = ClimbingController.MustUnmount();
        }
    }

    public void SetInputStatusObject(GameInputStatus inputStatus)
    {
        _inputStatus = inputStatus;
    }

    public bool ClimberIsUpright() {
        return Vector3.Angle(Motor.CharacterUp, Vector3.up) < 1;
    }

    public Vector3 CharacterUp() {
        return ModelRoot.up;
    }

    public Vector3 CharacterForward() {
        return ModelRoot.forward;
    }

    public Vector3 CharacterRight() {
        return ModelRoot.right;
    }

    private void OnTriggerEnter(Collider other)
    {
        OnEnterTriggerChecks(other);
        OnStayTriggerChecks(other);
    }

    private void OnTriggerStay(Collider other)
    {
        OnStayTriggerChecks(other);
    }

    private void OnStayTriggerChecks(Collider other)
    {
        if (other.transform.tag != "ClimbTrigger") return;
        NextMoveData.ClimbCandidate = other;
        if (State.IsInState(ClimberStates.Climbing)) ClimbingController.OnTriggerStay(other);
    }

    private void OnEnterTriggerChecks(Collider other)
    {
        if (State.IsInState(ClimberStates.Climbing)) ClimbingController.OnTriggerEnter(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (State.IsInState(ClimberStates.Climbing)) ClimbingController.OnTriggerExit(other);
    }

    /*********************************************
                       BELOW:
        KINEMATIC CHARACTER CONTROLLER OVERRIDES
    **********************************************/

    /// This is called before the character begins its movement update
    public void BeforeCharacterUpdate(float delta)
    {
        Cooldowns.Tick(delta);
    }

    /// This is the ONLY place where you can set the character's velocity
    public void UpdateVelocity(ref Vector3 currentVelocity, float delta)
    {
        Position = transform.position;
        currentVelocity = FindNewVelocity(currentVelocity, delta);
        PreviousPosition = transform.position;
        Motor.SetPosition(Position);

    }

    /// This is the ONLY place where you should set the character's rotation
    public void UpdateRotation(ref Quaternion currentRotation, float deltaTime)
    {
        // currentRotation = Rotation;
        ModelRoot.transform.rotation = Rotation;
    }

    public void OnGroundHit(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, ref HitStabilityReport hitStabilityReport)
    {
        NextMoveData.GroundHit = true;
    }

    public bool IsColliderValidForCollisions(Collider coll)
    {
        if (ClimbingController.GetMount() == coll) return false;
        return true;
    }

    public void ProcessHitStabilityReport(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, Vector3 atCharacterPosition, Quaternion atCharacterRotation, ref HitStabilityReport hitStabilityReport)
    {
        if (Settings.DebugSettings.ShowSurfaceTangents)
        {
            Color color = hitStabilityReport.IsStable ? Color.green : Color.red;
            Vector3 direction = Motor.GetDirectionTangentToSurface(Velocity, hitNormal);
            Debug.DrawLine(hitPoint, hitPoint + direction * 0.5f, color, 2f);
        }
    }

    protected void OnLeaveStableGround() { }
    public void OnDiscreteCollisionDetected(Collider hitCollider) { }
    public void OnMovementHit(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, ref HitStabilityReport hitStabilityReport) { }
    public void PostGroundingUpdate(float deltaTime) { }
    public void AfterCharacterUpdate(float deltaTime) { }
}