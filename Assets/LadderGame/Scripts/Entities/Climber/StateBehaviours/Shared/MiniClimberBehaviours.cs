﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniClimberBehaviours
{
    public static void ReorientClimberUpright(ClimberController c) {
        Quaternion q = Quaternion.FromToRotation(c.CharacterUp(), Vector3.up) * c.Rotation;
        c.Rotation = Quaternion.Lerp(c.Rotation, q, Time.deltaTime * 8f);
    }

    public static void ReorientToGivenOrientation(ClimberController c, Quaternion target, float speed) {
        c.Rotation = Quaternion.Lerp(c.Rotation, target, Time.deltaTime * speed);
    }

    public static void FaceDirection(ClimberController c, Vector2 direction) {
        c.Rotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.y), Vector3.up);
    }
}
