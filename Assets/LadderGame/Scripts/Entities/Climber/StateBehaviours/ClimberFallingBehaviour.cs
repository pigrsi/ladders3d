﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimberFallingBehaviour : ClimberStateBehaviour {
    public ClimberFallingBehaviour(ClimberController c) : base(c) {}

    public override void Update(float delta) {
        Vector2 moveAxis = c.NextMoveData.CamMoveAxis;
        Vector3 AirVelocity = c.Velocity + new Vector3(moveAxis.x, 0, moveAxis.y) * c.AirControlDegree;
        AirVelocity.y = 0;
        AirVelocity = Vector3.ClampMagnitude(AirVelocity, c.MaxAirSpeed);
        AirVelocity.y = c.Velocity.y;

        if (!c.Motor.GroundingStatus.IsStableOnGround && c.Motor.GroundingStatus.FoundAnyGround) {
            AirVelocity.y -= 20 * delta;
        }

        c.Velocity = AirVelocity;

        ApplyGravity(delta);
        
        if (!c.ClimberIsUpright()) MiniClimberBehaviours.ReorientClimberUpright(c); 
        else if (moveAxis.magnitude > 0) MiniClimberBehaviours.FaceDirection(c, moveAxis);
    }

    private void ApplyGravity(float delta) {
        c.Velocity += (c.UsePhysicsGravity ? Physics.gravity : c.Gravity) * delta;
    }
}