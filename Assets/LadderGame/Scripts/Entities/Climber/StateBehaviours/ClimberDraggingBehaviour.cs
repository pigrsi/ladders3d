﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimberDraggingBehaviour : ClimberStateBehaviour {
    public ClimberDraggingBehaviour(ClimberController c) : base(c) {}

    private Transform _grabbedObject;
    private HingeJoint _leftJoint;
    private HingeJoint _rightJoint;
    private enum DragMode { None, Pushing, Pulling, BouncedOffObject };
    private DragMode _dragMode = DragMode.None;
    private float _initialGrabbedAngle;

    public void CheckDragState() {
        if (c.GrabBehaviour.IsHoldingObject()) return;

        Transform grabbable = c.GrabBehaviour.GetLiftableIfExists(c);
        if (grabbable != null) c.GrabBehaviour.GrabGrabbable(grabbable);
    }

    public void Grab(Transform grabbable) {
        _grabbedObject = grabbable;
        Rigidbody leftHand = c.LeftHand.gameObject.GetComponent<Rigidbody>();
        Rigidbody rightHand = c.RightHand.gameObject.GetComponent<Rigidbody>();
        Rigidbody otherBody = grabbable.GetComponent<Rigidbody>();

        _leftJoint = SetupJoint(leftHand, otherBody);
        _rightJoint = SetupJoint(rightHand, otherBody);

        _initialGrabbedAngle = GetGrabbedAngle();
    }

    private float GetGrabbedAngle() {
        Vector3 toLadder = Vector3.ProjectOnPlane(c.transform.position - _grabbedObject.position, Vector3.up);
        return Vector3.Angle(c.transform.forward, toLadder);
    }

    private HingeJoint SetupJoint(Rigidbody hand, Rigidbody grabbable) {
        HingeJoint joint = hand.gameObject.AddComponent(typeof(HingeJoint)) as HingeJoint;
        joint.connectedBody = grabbable;
        joint.enablePreprocessing = false;
        joint.axis = Vector3.right;
        return joint;
    }

    public override void Update(float delta) {
        Vector2 moveAxis = c.NextMoveData.CamMoveAxis;
        c.Velocity = new Vector3(moveAxis.x * c.MaxWalkSpeed, c.Velocity.y, moveAxis.y * c.MaxWalkSpeed);

        if (c.Motor.GroundingStatus.FoundAnyGround) {
            float magni = c.Velocity.magnitude;
            c.Velocity = magni * c.Motor.GetDirectionTangentToSurface(c.Velocity, c.Motor.GroundingStatus.GroundNormal);
        } else {
            c.Velocity += c.Gravity;
        }

        float IncorrectDragAngleAllowance = 10f;
        if (_dragMode != DragMode.BouncedOffObject && Mathf.Abs(_initialGrabbedAngle - GetGrabbedAngle()) > IncorrectDragAngleAllowance) {
            _dragMode = DragMode.BouncedOffObject;
            c.Cooldowns.Add("DragBounceOffObject", 0.1f);
        }

        SetRotationFromMovement(moveAxis);
    }

    private void SetRotationFromMovement(Vector2 moveAxis) {
        if (_dragMode == DragMode.BouncedOffObject) BounceOffObject();
        else NormalRotation(moveAxis);        
    }

    private void BounceOffObject() {
        if (!c.Cooldowns.IsActive("DragBounceOffObject")) {
            _dragMode = DragMode.None;            
        } else {
            // c.Velocity = new Vector3(0, c.Velocity.y, 0);
            c.Rotation *= Quaternion.Euler(0, -3, 0);
        }
    }

    private void NormalRotation(Vector2 moveAxis) {
        if (moveAxis.magnitude == 0) _dragMode = DragMode.None;
        if (c.Velocity.magnitude == 0) return;

        if (_dragMode == DragMode.None) SetDragMode(moveAxis);

        Vector3 lookVector;
        if (_dragMode == DragMode.Pulling) {
            lookVector = Vector3.ProjectOnPlane(c.Velocity * -1, Vector3.up);
        } else {
            lookVector = Vector3.ProjectOnPlane(c.Velocity, Vector3.up);
        }

        Quaternion desiredRotation = Quaternion.LookRotation(lookVector);
        float DraggingRotateSpeed = 140f;
        c.Rotation = Quaternion.RotateTowards(c.Rotation, desiredRotation, DraggingRotateSpeed * Time.deltaTime);
    }

    private void SetDragMode(Vector2 moveAxis) {
        Vector3 moveAxisPointingAt = new Vector3(moveAxis.x, 0, moveAxis.y);
        if (Mathf.Abs(Vector3.Angle(moveAxisPointingAt, c.transform.forward)) > 130) _dragMode = DragMode.Pulling;
        else _dragMode = DragMode.Pushing;
    }
}