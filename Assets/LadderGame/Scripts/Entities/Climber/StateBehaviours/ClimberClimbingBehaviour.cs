﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class ClimberClimbingBehaviour : ClimberStateBehaviour {
        public ClimberClimbingBehaviour(ClimberController c) : base(c) { _climbBehaviour = new ClimbingBehaviour(c.transform); }
        private ClimbingBehaviour _climbBehaviour;

        public void JustEnteredState(Collider mount) {
            c.Velocity = Vector3.zero;
            c.State.ChangeState(ClimberStates.Climbing);
            c.Motor.ForceUnground();
            _climbBehaviour.MountLadder(mount);
            Update(Time.deltaTime);
        }

        public bool MustUnmount() {
            return !_climbBehaviour.IsStillWithinGripRange();
        }

        public override void Update(float deltaTime) {
            if (!c.State.IsInState(ClimberStates.Climbing)) return;
            Vector2 rawMoveAxis = c.NextMoveData.RawMoveAxis;
            // c.Rotation = _climbBehaviour.GetMountedRotation();
            MiniClimberBehaviours.ReorientToGivenOrientation(c, _climbBehaviour.GetMountedRotation(), 20f);
            if (rawMoveAxis.magnitude != 0) _climbBehaviour.MoveAlong(rawMoveAxis.y, c.MaxClimbSpeed);
            c.Velocity = c.Motor.GetVelocityForMovePosition(c.transform.position, _climbBehaviour.GetLadderSnapPosition(), deltaTime);
        }

        public void Unmount(bool jumpDismount) {
            _climbBehaviour.UnmountLadder();
            Vector3 unmountVelocity = _climbBehaviour.GetUnmountVelocity();
            if (jumpDismount) c.Velocity += unmountVelocity;
            else c.Velocity = unmountVelocity;
            c.Cooldowns.Add("Mount", 0.5f);
        }

        public void OnTriggerEnter(Collider other) {
            _climbBehaviour.OnTriggerEnter(other);
        }

        public void OnTriggerStay(Collider other) {
            _climbBehaviour.OnTriggerStay(other);
        }

        public void OnTriggerExit(Collider other) {
            _climbBehaviour.OnTriggerExit(other);
        }

        public Collider GetMount() {
            return _climbBehaviour.GetMount();
        }
    }