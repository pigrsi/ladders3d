﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimberLiftingBehaviour : ClimberStateBehaviour
{
    public ClimberLiftingBehaviour(ClimberController c) : base(c) {
        _liftingBehaviour = new LiftingHandsBehaviour(c);
        _ladderCollision = new LiftingLadderCollision(c);
    }
    private LiftingHandsBehaviour _liftingBehaviour;
    private LiftingLadderCollision _ladderCollision;
    private GameObject _ladderObject;
    private Vector3 _previousForward;
    private Vector2 _currentWalkVelocity;
    private float _currentRotateSpeed;

    public override void Update(float delta) {
        if (c.Motor.GroundingStatus.FoundAnyGround) {
            Walk(delta);
        } else {
            ApplyGravity(delta);
        }

        _liftingBehaviour.UpdateInfo(_ladderCollision.IsColliding());
        _liftingBehaviour.Update(delta);
        
        _ladderCollision.HandleLadderCollision();
        HandleRotation(delta);

        _previousForward = c.CharacterForward();
    }

    public override void FixedUpdate() {
        _ladderCollision.FixedUpdate();
    }

    public void StartGrab(Transform ladder) {
        Reset();
        _ladderObject = ladder.gameObject;
        _liftingBehaviour.StartGrab(ladder);
        _previousForward = c.CharacterForward();
        _ladderCollision.OnGrab(ladder, _liftingBehaviour.GetLeftJoint());
    }

    public void DropLadder() {
        _liftingBehaviour.DropLadder();
        _ladderCollision.OnDrop(_ladderObject);
    }

    private void Reset() {
        _currentWalkVelocity = Vector2.zero;
        _currentRotateSpeed = 0;
    }

    public void HandleRotation(float delta) {
        float target = 0;
        if (c.NextMoveData.RotateAxis > 0) target = c.MaxLiftingRotateSpeed;
        if (c.NextMoveData.RotateAxis < 0) target = -c.MaxLiftingRotateSpeed;
        _currentRotateSpeed = Mathf.MoveTowards(_currentRotateSpeed, target, c.LiftingRotateAcceleration * delta);

        if (_ladderCollision.LeftRotationBlocked()) {
            if (_liftingBehaviour.IsPickingUpLadder()) _currentRotateSpeed = 120;
            else if (_currentRotateSpeed <= 0) _currentRotateSpeed *= -1;
            addExtraRotateSpeedFromWalking(false);
        }
        if (_ladderCollision.RightRotationBlocked()) {
            if (_liftingBehaviour.IsPickingUpLadder()) _currentRotateSpeed = -120;
            else if (_currentRotateSpeed >= 0) _currentRotateSpeed *= -1;
            addExtraRotateSpeedFromWalking(true);
        } 

        void addExtraRotateSpeedFromWalking(bool rotateLeft) {
            Vector2 moveAxis = c.NextMoveData.CamMoveAxis;
            float multiplier = rotateLeft ? -1 : 1;
            Vector3? collisionNormal = _ladderCollision.GetLadderCollisionNormal();
            if (collisionNormal.HasValue) {
                Vector2 flatNormal = new Vector2(collisionNormal.Value.x, collisionNormal.Value.z);
                if (Vector3.Cross(moveAxis, Vector2.Perpendicular(flatNormal)).z < 0) _currentRotateSpeed += (20 * multiplier);
            }
        }

        c.Rotation *= Quaternion.Euler(0, _currentRotateSpeed * delta, 0);
    }

    private void Walk(float delta) {
        Vector2 moveAxis = c.NextMoveData.CamMoveAxis;
        Vector3? collisionNormal = _ladderCollision.GetLadderCollisionNormal();
        bool allowRequestedDirection = true;
        if (collisionNormal.HasValue) {
            // Bounce off by walking in direction of normal
            Vector2 normalWalkDirection = new Vector2(collisionNormal.Value.x, collisionNormal.Value.z);            
            // Player can still move by their input if it's pointing in a similar direction to the normal
            allowRequestedDirection = Vector3.Cross(moveAxis, Vector2.Perpendicular(normalWalkDirection)).z > -0.1f;

            _currentWalkVelocity = normalWalkDirection * _currentWalkVelocity.magnitude;
        }

        if (allowRequestedDirection) {
            _currentWalkVelocity = Vector2.MoveTowards(_currentWalkVelocity, moveAxis * c.MaxLiftingWalkSpeed, c.LiftingWalkAcceleration * delta);
        }

        c.Velocity = new Vector3(_currentWalkVelocity.x, 0, _currentWalkVelocity.y);
        float magni = _currentWalkVelocity.magnitude;
        c.Velocity = magni * c.Motor.GetDirectionTangentToSurface(c.Velocity, c.Motor.GroundingStatus.GroundNormal);
    }

    private void ApplyGravity(float delta) {
        c.Velocity += (c.UsePhysicsGravity ? Physics.gravity : c.Gravity) * delta;
    }
}
