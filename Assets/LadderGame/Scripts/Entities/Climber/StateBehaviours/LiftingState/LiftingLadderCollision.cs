﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftingLadderCollision
{
    private ClimberController c;
    private Collision _ladderCollision;
    private Quaternion _lastGoodRotation;
    private Joint _liftJoint;
    private bool _leftRotationBlocked;
    private bool _rightRotationBlocked;
    private bool _handledLastCollision = true;
    private float _initialSleepThreshold;
    private enum RotateDirection {LEFT, RIGHT};
    
    public LiftingLadderCollision(ClimberController c) {
        this.c = c;
    }

    public void FixedUpdate() {
        if (_handledLastCollision) {
            _ladderCollision = null;
            _lastGoodRotation = c.Rotation;
        } 
    }

    public void OnLadderCollision(Collision collision) {
        ContactPoint[] contacts = new ContactPoint[collision.contactCount];
        collision.GetContacts(contacts);
        for (int i = 0; i < contacts.Length; ++i) {
            if (contacts[i].normal.y < 0.8f) {
                _ladderCollision = collision;
                _handledLastCollision = false;
                return;
            }
        }
    }

    public Vector3? GetLadderCollisionNormal() {
        if (_ladderCollision == null) return null;
        if (_ladderCollision.impulse.normalized.magnitude > 0) return _ladderCollision.impulse.normalized;
        return null;
    }

    public void HandleLadderCollision() {
        if (_ladderCollision != null) {
            c.Rotation = _lastGoodRotation;
            ContactPoint contact = _ladderCollision.GetContact(0);
            Vector3 normal = _ladderCollision.GetContact(0).normal;
        //     if (_ladderCollision.impulse.normalized.y < 0.5) {
                Vector3 flatNormal = Vector3.ProjectOnPlane(_ladderCollision.impulse.normalized, Vector3.up);
                Debug.DrawLine(_ladderCollision.GetContact(0).point, _ladderCollision.GetContact(0).point + flatNormal * 5, Color.cyan, 0.5f);
                Vector3 cross = Vector3.Cross(c.CharacterForward(), flatNormal);
                 Debug.DrawLine(_ladderCollision.GetContact(0).point, _ladderCollision.GetContact(0).point + cross * 5, Color.yellow, 0.5f);
                if (cross.y > 0) {
                     _leftRotationBlocked = true;
                     _rightRotationBlocked = false;
                } else if (cross.y < 0) {
                    _leftRotationBlocked = false;
                    _rightRotationBlocked = true;
                }
                _handledLastCollision = true;
        } else {
            _rightRotationBlocked = false;
            _leftRotationBlocked = false;
        }
    }

    public void OnGrab(Transform ladder, Joint liftJoint) {
        _liftJoint = liftJoint;
        ListenForCollisions script = ladder.gameObject.AddComponent<ListenForCollisions>();
        script.SetCallbacks(OnLadderCollision, OnLadderCollision);
        Rigidbody ladderBody = ladder.GetComponent<Rigidbody>();
        _initialSleepThreshold = ladderBody.sleepThreshold;
        ladderBody.sleepThreshold = 0;

        _lastGoodRotation = c.Rotation;
        _leftRotationBlocked = false;
        _rightRotationBlocked = false;
        _handledLastCollision = true;
    }

    public void OnDrop(GameObject ladder) {
        ladder.GetComponent<Rigidbody>().sleepThreshold = _initialSleepThreshold;
        Object.Destroy(ladder.GetComponent<ListenForCollisions>());
    }

    public bool LeftRotationBlocked() { return _leftRotationBlocked; }
    public bool RightRotationBlocked() { return _rightRotationBlocked; }
    public bool IsColliding() { return _ladderCollision != null; }
}
