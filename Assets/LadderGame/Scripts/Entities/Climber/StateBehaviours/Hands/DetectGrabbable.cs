﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectGrabbable : MonoBehaviour
{
    [HideInInspector] private List<Transform> Grabbables = new List<Transform>();

    void OnTriggerEnter(Collider other) {
        if (Utility.IsGrabbable(other.transform)) Grabbables.Add(other.transform);
    }

    void OnTriggerExit(Collider other) {
        if (Grabbables.Contains(other.transform)) Grabbables.Remove(other.transform);
    }

    public bool HasGrabbableInRange() {
        return Grabbables.Count > 0;
    }

    public List<Transform> GetGrabbables() {
        return Grabbables;
    }

    public bool HasDetectedGrab(Transform grabbable) {
        return Grabbables.Contains(grabbable);
    }
}
