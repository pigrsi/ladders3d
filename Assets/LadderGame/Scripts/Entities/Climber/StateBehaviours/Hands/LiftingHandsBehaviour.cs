using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftingHandsBehaviour : ClimberStateBehaviour
{
    private Transform _ladder;
    private Joint _leftJoint, _rightJoint;
    public const float SpaceBetweenHandsWhenLifting = 2;
    private HandStates _state = HandStates.Free;
    private Transform _handPivot;
    private Transform _leftHand;
    private Transform _rightHand;
    private CooldownManager _cooldowns = new CooldownManager();
    private struct TargetTransform
    {
        public Quaternion InitialLocalOrientation;
        public Vector3 InitialLocalPosition;
        public Quaternion TargetLocalOrientation;
        public Vector3 TargetLocalPosition;
    }
    private TargetTransform _currentTarget;
    private bool LadderIsColliding;

    public LiftingHandsBehaviour(ClimberController c) : base(c)
    {
        _handPivot = c.HandPivot;
        _leftHand = c.LeftHand;
        _rightHand = c.RightHand;
    }

    public override void Update(float delta)
    {
        _cooldowns.Tick(delta);
        if (_state == HandStates.Free)
        {
            _leftHand.localPosition = new Vector3(-1, 0, 0);
            _rightHand.localPosition = new Vector3(1, 0, 0);
        }
        else if (_state == HandStates.PickUpLadder)
        {
            UpdatePickUpLadder(delta);
        }
        else if (_state == HandStates.Lifting)
        {
            UpdateLifting(delta);
        }
    }

    public void UpdateInfo(bool ladderIsColliding) {
        LadderIsColliding = ladderIsColliding;
    }

    public void UpdatePickUpLadder(float deltaTime) {
        float deltaSpeed = 5f * deltaTime;
        c.HandPivot.localRotation = Quaternion.Lerp(c.HandPivot.localRotation, _currentTarget.TargetLocalOrientation, deltaSpeed);
        c.HandPivot.localPosition = Vector3.Lerp(c.HandPivot.localPosition, _currentTarget.TargetLocalPosition, deltaSpeed);
        if (Quaternion.Angle(c.HandPivot.localRotation, _currentTarget.TargetLocalOrientation) < 0.01f &&
            Vector3.Distance(c.HandPivot.localPosition, _currentTarget.TargetLocalPosition) < 0.01f) {
            ChangeState(HandStates.Lifting);
        }
    }

    public void UpdateLifting(float deltaTime)
    {

    }

    public void ChangeState(HandStates newState)
    {
        _state = newState;

        if (newState == HandStates.PickUpLadder)
        {
            _currentTarget = CalculateLiftHandTarget();
        }
        else if (newState == HandStates.Lifting)
        {
            _leftJoint.axis = _leftHand.InverseTransformDirection(_ladder.right);
            _rightJoint.axis = _leftJoint.axis;
            ResetPivot();
        }
    }

    public void StartGrab(Transform ladder)
    {
        _leftHand.transform.localRotation = Quaternion.identity;
        _rightHand.transform.localRotation = Quaternion.identity;
        bool grabTop = ShouldGrabTopEnd(ladder, c.ModelRoot.transform);
        // bool grabbingFront = GrabbingFrontOfLadder(ladder, grabTop);
        Vector3 left = GetHandGrabPosition(ladder, false, grabTop);
        Vector3 right = GetHandGrabPosition(ladder, true, grabTop);
        if (ShouldSwapLeftAndRight(ladder, c.ModelRoot.transform))
        {
            Vector3 temp = left;
            left = right;
            right = temp;
        }

        _leftHand.transform.position = left;
        _rightHand.transform.position = right;

        // Now in position, create the joints
        _ladder = ladder;
        CreateJoints();
        ChangeState(HandStates.PickUpLadder);
    }

    private void CreateJoints()
    {
        Joint createSingleJoint(Rigidbody hand, Rigidbody ladder)
        {
            HingeJoint joint = hand.gameObject.AddComponent(typeof(HingeJoint)) as HingeJoint;
            joint.anchor = Vector3.zero;
            joint.connectedBody = ladder;
            joint.enablePreprocessing = false;
            joint.axis = Vector3.right;
            return joint;
        }

        Rigidbody leftHandBody = c.LeftHand.gameObject.GetComponent<Rigidbody>();
        Rigidbody rightHandBody = c.RightHand.gameObject.GetComponent<Rigidbody>();
        Rigidbody ladderBody = _ladder.GetComponent<Rigidbody>();

        _leftJoint = createSingleJoint(leftHandBody, ladderBody);
        _rightJoint = createSingleJoint(rightHandBody, ladderBody);
    }

    private void BreakJoints()
    {
        Object.Destroy(_leftJoint);
        Object.Destroy(_rightJoint);
    }

    private void ResetPivot()
    {
        _leftHand.SetParent(null, true);
        _rightHand.SetParent(null, true);
        _handPivot.localPosition = Vector3.zero;
        _handPivot.localRotation = Quaternion.identity;
        _leftHand.SetParent(_handPivot);
        _rightHand.SetParent(_handPivot);
    }

    public void DropLadder()
    {
        if (_leftJoint != null) { Object.Destroy(_leftJoint); _leftJoint = null; }
        if (_rightJoint != null) { Object.Destroy(_rightJoint); _rightJoint = null; }
        ChangeState(HandStates.Free);
    }

    public Joint GetLeftJoint()
    {
        return _leftJoint;
    }

    public bool IsPickingUpLadder() {
        return _state == HandStates.PickUpLadder;
    }

    /*************************** HELPER FUNCTIONS *****************************/
    private static bool ShouldSwapLeftAndRight(Transform ladder, Transform climber)
    {
        return Vector3.Angle(ladder.right, climber.right) > 90;
    }

    private static bool ShouldGrabTopEnd(Transform ladder, Transform climber)
    {
        BoxCollider ladderCollider = ladder.GetComponent<BoxCollider>();

        Vector3 startPoint = ladder.transform.position;
        Vector3 halfLengthVector = (ladder.forward * ladderCollider.size.z / 2);
        Vector3 topEnd = startPoint + halfLengthVector;
        Vector3 bottomEnd = startPoint - halfLengthVector;

        return Vector3.Distance(climber.position, topEnd) < Vector3.Distance(climber.position, bottomEnd);
    }

    private static Vector3 GetHandGrabPosition(Transform ladder, bool isRightHand, bool grabbingTop)
    {
        const float spaceBetweenHands = 2f;
        int directionMultiplier = grabbingTop ? 1 : -1;
        int handDirectionMultiplier = isRightHand ? 1 : -1;

        Vector3 endPoint = GetLadderEndPoint(ladder, grabbingTop);
        endPoint -= ladder.forward * directionMultiplier * 0.5f;
        return endPoint + (ladder.right * handDirectionMultiplier * spaceBetweenHands / 2);
    }

    private static Vector3 GetLadderEndPoint(Transform ladder, bool topEnd)
    {
        BoxCollider ladderCollider = ladder.GetComponent<BoxCollider>();
        int directionMultiplier = topEnd ? 1 : -1;
        Vector3 startPoint = ladder.transform.position;
        return startPoint + (ladder.forward * directionMultiplier * ladderCollider.size.z / 2);
    }

    private float CalculateLiftAngle(Transform ladder)
    {
        Vector3 climberUp = c.CharacterUp();
        Vector3 ladderUp = ladder.up;

        ladderUp = Vector3.ProjectOnPlane(ladderUp, c.CharacterRight());
        ladderUp.y = Mathf.Abs(ladderUp.y);

        return Vector3.Angle(climberUp, ladderUp);
    }

    private TargetTransform CalculateLiftHandTarget()
    {
        TargetTransform target = new TargetTransform();
        target.InitialLocalOrientation = c.HandPivot.localRotation;
        target.InitialLocalPosition = c.HandPivot.localPosition;

        void rotation1()
        {
            // Keep hands level on horizontal axes
            bool leftAboveRight = _leftHand.position.y > _rightHand.position.y;
            Vector3 betweenHands = _rightHand.position - _leftHand.position;
            Vector3 flatAxis = Vector3.ProjectOnPlane(betweenHands, Vector3.up);
            float angleToRotate = -Vector3.Angle(betweenHands, flatAxis);
            if (leftAboveRight) angleToRotate *= -1;

            c.HandPivot.rotation *= Quaternion.Euler(0, 0, angleToRotate);
        }

        void rotation2()
        {
            // Rotate hands to correct place about Y axis
            Vector3 betweenHands = _rightHand.position - _leftHand.position;
            betweenHands = Vector3.ProjectOnPlane(betweenHands, c.CharacterUp());
            float angleToRotate = Vector3.Angle(c.CharacterRight(), betweenHands);
            Vector3 cross = Vector3.Cross(c.CharacterRight(), betweenHands);
            if (cross.y < 0) angleToRotate *= -1;

            c.HandPivot.rotation *= Quaternion.Euler(0, -angleToRotate, 0);
        }

        void movement1()
        {
            // Now hands are oriented correctly, move them to the right position
            // Move pivot instead of the hands individually
            Vector3 desiredRightHandPos = c.transform.position + (c.CharacterForward() * 1.75f) + c.CharacterRight() + c.CharacterUp() * 0.25f;
            Vector3 moveBy = desiredRightHandPos - _rightHand.position;

            c.HandPivot.position += moveBy;
        }

        rotation1();
        rotation2();
        movement1();

        target.TargetLocalOrientation = c.HandPivot.localRotation;
        target.TargetLocalPosition = c.HandPivot.localPosition;
        c.HandPivot.localRotation = target.InitialLocalOrientation;
        c.HandPivot.localPosition = target.InitialLocalPosition;
        return target;
    }
}