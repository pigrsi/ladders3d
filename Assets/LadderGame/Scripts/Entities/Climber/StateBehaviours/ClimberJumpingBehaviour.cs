﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimberJumpingBehaviour : ClimberStateBehaviour
{
    public ClimberJumpingBehaviour(ClimberController c) : base(c) {}

    public override void Update(float delta) {
        c.Motor.ForceUnground();
        c.Velocity = new Vector3(c.Velocity.x, c.JumpSpeed, c.Velocity.z);
    }
}
