﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ClimberStateBehaviour {
    protected ClimberController c;
    public ClimberStateBehaviour(ClimberController c) {
        this.c = c;
    }

    public abstract void Update(float delta);
    public virtual void FixedUpdate() {}
}
