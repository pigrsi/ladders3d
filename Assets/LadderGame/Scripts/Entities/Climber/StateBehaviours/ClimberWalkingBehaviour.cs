﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimberWalkingBehaviour : ClimberStateBehaviour {
    public ClimberWalkingBehaviour(ClimberController c) : base(c) {}
    
    public override void Update(float delta) {
        Vector2 moveAxis = c.NextMoveData.CamMoveAxis;
        c.Velocity = new Vector3(moveAxis.x * c.MaxWalkSpeed, c.Velocity.y, moveAxis.y * c.MaxWalkSpeed);

        if (c.Motor.GroundingStatus.FoundAnyGround) {
            float magni = c.Velocity.magnitude;
            c.Velocity = magni * c.Motor.GetDirectionTangentToSurface(c.Velocity, c.Motor.GroundingStatus.GroundNormal);
        }

        if (moveAxis.magnitude > 0) MiniClimberBehaviours.FaceDirection(c, moveAxis);
        // if (Vector3.Angle(c.CharacterUp(), Vector3.up) > 0.002) MiniClimberBehaviours.ReorientClimberUpright(c); 
    }
}