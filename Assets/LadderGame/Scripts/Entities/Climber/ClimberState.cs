﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimberState
{
    public ClimberStates Current = ClimberStates.Falling;

    public ClimberStates ChangeState(ClimberStates newState) {
        return (Current = newState);
    }

    public ClimberStates SetStateFromMoveData(ClimberController.ClimberNextMoveData data) {
        if (IsInState(ClimberStates.Idle)) return SetStateFromIdle(data);
        else if (IsInState(ClimberStates.Walking)) return SetStateFromWalking(data);
        else if (IsInState(ClimberStates.Jumping)) return SetStateFromJumping(data);
        else if (IsInState(ClimberStates.Falling)) return SetStateFromFalling(data);
        else if (IsInState(ClimberStates.Climbing)) return SetStateFromClimbing(data);
        else if (IsInState(ClimberStates.Dragging)) return SetStateFromDragging(data);
        else if (IsInState(ClimberStates.Lifting)) return SetStateFromLifting(data);
        return ClimberStates.Null;
    }

    private ClimberStates SetStateFromIdle(ClimberController.ClimberNextMoveData data) {
        if (data.StableOnGround) {
            if (CanLiftAndIsAble(data)) {
                return ChangeState(ClimberStates.Lifting);
            } else if (data.JumpRequested) {
                // Tried to jump while on the ground, so Jump
                return ChangeState(ClimberStates.Jumping);
            } else if (ClimberIsMovingLaterallyOrTryingTo(data)) {
                // Moving or trying to move while on ground, so start Walking
                return ChangeState(ClimberStates.Walking);
            } else {
                return ClimberStates.Null;
            }
        } else {
             return ClimberStates.Null;
        }
    }

    private ClimberStates SetStateFromWalking(ClimberController.ClimberNextMoveData data) {
        if (CanLiftAndIsAble(data)) {
            return ChangeState(ClimberStates.Lifting);
        }

        if (data.ClimbCandidate != null && !data.Cooldowns.IsActive("Mount")) {
            // There is something I can climb so start Climbing
            return ChangeState(ClimberStates.Climbing);
        }
        
        if (data.StableOnGround) {
            if (data.JumpRequested) {
                // Tried to jump while on the ground, so Jump
                return ChangeState(ClimberStates.Jumping);
            } else if (!ClimberIsMovingLaterallyOrTryingTo(data)) {
                // Not moving so become Idle
                return ChangeState(ClimberStates.Idle);
            } else {
                return ClimberStates.Null;
            }
        } else {
            // Not stable so fall
            return ChangeState(ClimberStates.Falling);
        }
    }

    private ClimberStates SetStateFromJumping(ClimberController.ClimberNextMoveData data) {
        // Jumping currently always takes one frame so instantly switch to Falling
        return ChangeState(ClimberStates.Falling);
    }

    private ClimberStates SetStateFromFalling(ClimberController.ClimberNextMoveData data) {
        if (data.ClimbCandidate != null && !data.Cooldowns.IsActive("Mount")) {
            // There is something I can climb so start Climbing
            return ChangeState(ClimberStates.Climbing);
        } else if (data.StableOnGround) {
            if (data.IsUpright) {
                if (ClimberIsMovingLaterallyOrTryingTo(data)) {
                    // Landed and moving or trying to move so start Walking
                    return ChangeState(ClimberStates.Walking);
                } else {
                    // Landed and not moving or trying to move so become Idle
                    return ChangeState(ClimberStates.Idle);
                }
            } else {
                return ClimberStates.Null;
            }
        } else {
            return ClimberStates.Null;
        }
    }

    private ClimberStates SetStateFromClimbing(ClimberController.ClimberNextMoveData data) {
        if (data.StableOnGround) {
            // if (ClimberIsMovingLaterallyOrTryingTo(data)) {
            //     // Climber is moving or trying to on the ground, so start Walking
            //     return ChangeState(ClimberStates.Walking);
            // } else {
            //     // Climber is not moving or trying to, on the ground, so become Idle
            //     return ChangeState(ClimberStates.Idle);
            // }
            return ChangeState(ClimberStates.Falling);
        } else if (data.MustUnmount) {
            // Climber is forced to unmount, Fall off mount
            return ChangeState(ClimberStates.Falling);
        } else if (data.JumpRequested) {
            // Climber wants to jump off mount, so start Jumping
            return ChangeState(ClimberStates.Jumping);
        } else {
            return ClimberStates.Null;
        }
    }

    private ClimberStates SetStateFromDragging(ClimberController.ClimberNextMoveData data) {
        return ClimberStates.Null;
    }

    private ClimberStates SetStateFromLifting(ClimberController.ClimberNextMoveData data) {
        if (data.GrabRequested) {
            return ChangeState(ClimberStates.Walking);
        }
        return ClimberStates.Null;
    }

    public ClimberStates GetCurrentState() {
        return Current;
    }

    public bool IsInState(ClimberStates state) {
        return Current == state;
    }

    // Helpers
    private static bool ClimberIsMovingLaterallyOrTryingTo(ClimberController.ClimberNextMoveData data) {
        return data.CamMoveAxis.magnitude > 0 || data.Velocity.x != 0 || data.Velocity.z != 0;
    }

    private static bool CanLiftAndIsAble(ClimberController.ClimberNextMoveData data) {
        return data.GrabRequested && data.LiftCandidate != null;
    }
}
