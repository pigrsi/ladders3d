﻿using UnityEngine;

public enum ClimberStates
{
    Null, Idle, Walking, Jumping, Falling, Climbing, Dragging, Lifting
}
